## ¿Cómo crear el ambiente de desarrollo?

**Debe renombrar el archivo .env.example por .env ahi colocar sus datos de conexión a postgres.**

1. Deberas tener instalado python 3.6 y pip3
2. Clonar el repositorio.
3. Dentro del directorio del proyecto ejecutar. 
    ```
    $: pip3 install -r requirements.txt
    ```
4. El siguiente comando crea la estrucutra de base de datos.
   ```
   $: python3 manage.py makemigrations system
   $: python3 manage.py migrate_schemas
   ```
5. Carga los datos de desarrollo.
   ```
   $: python3 manage.py config_sys
   ```
6. Ahora podrá crear la estructura de base de datos de todo del sistema ejecutando los siguientes 2 comandos.
   ```
   $: python3 manage.py makemigrations verification
   $: python3 manage.py migrate_schemas --schema=oficina_dev
   ```
7. Ejecutamos el servidor de desarrollo
   ```
   $: python3 manage.py runserver oficina.localhost:8000
   ```
   **Para ingresar:**
   ```
   usuario desarrollador
   Contraseña 12345678
   ```
### Obligatorio para ambiente de Producción
Colocarle un hash al secret_key de django
```
   $: python3 manage.py generate_key
 ```

 Colocar la clave en el archivo .env