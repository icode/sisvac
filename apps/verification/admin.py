from django.contrib import admin
from django.utils.safestring import mark_safe
from rangefilter.filters import DateRangeFilter

from apps.system.models import Offices
from apps.system.utils import email_request_accepted, email_request_rejected
from apps.verification.models import VerificationRequest, Applicant


@admin.register(Applicant)
class AdminApplicant(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False


@admin.register(VerificationRequest)
class AdminVerificationRequest(admin.ModelAdmin):
    list_display = ['id', 'applicant', 'status', 'application_date', 'acta']
    list_display_links = ['id', 'applicant']
    readonly_fields = ['show_birth_certificate', 'datos_del_solicitante', 'birth_certificate', 'application_date']
    list_filter = (
        ('status'),
        ('application_date', DateRangeFilter)
    )

    fieldsets = [
        ('Datos del solicitante', {
            'fields': [
                ('datos_del_solicitante',),
                ('application_date',),
                ('birth_certificate',),
                ('show_birth_certificate',),
                ('status',),
                ('date_appointment',)
            ]
        })
    ]

    def datos_del_solicitante(self, obj):
        if obj.applicant.second_name is None:
            second_name = ' - '
        else:
            second_name = obj.applicant.second_name

        if obj.applicant.mother_last_name is None:
            mother_last_name = ' - '
        else:
            mother_last_name = obj.applicant.mother_last_name

        return mark_safe('<strong>' + str(obj.applicant.identification_number) + ' - '
                         + obj.applicant.first_name + ' ' + second_name + ' '
                         + obj.applicant.last_name + ' ' + mother_last_name + '</strong>')

    def show_birth_certificate(self, obj):
        return mark_safe('<img src="{url}" width="250" height="350"/>'.format(
            url=obj.birth_certificate.url,
        )
        )

    def has_add_permission(self, request):
        return False

    def save_model(self, request, obj, form, change):
        if change:
            office = Offices.objects.get(users__username=request.users.username)
            if obj.status == 'A':
                email_request_accepted(
                    obj.applicant.email,
                    obj.applicant.first_name,
                    obj.date_appointment,
                    office.address
                )
            if obj.status == 'R':
                email_request_rejected(obj.applicant.email, obj.applicant.first_name)

        obj.save()
