import datetime as _date
from django.db import models
from django.utils.safestring import mark_safe
from django_softdelete.models import SoftDeleteModel


class Applicant(SoftDeleteModel):
    identification_number = models.CharField(max_length=40, primary_key=True, verbose_name='Cédula de identidad')
    first_name = models.CharField(max_length=40, verbose_name='Primer nombre')
    second_name = models.CharField(max_length=40, null=True, blank=True, verbose_name='Segundo nombre')
    last_name = models.CharField(max_length=40, verbose_name='Primer apellido')
    mother_last_name = models.CharField(max_length=40, null=True, blank=True, verbose_name='Segundo apellido')
    email = models.EmailField(max_length=100, null=False, blank=False, verbose_name='Correo electrónico')
    phone = models.CharField(max_length=11, null=False, blank=False, verbose_name='Número de celular')

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        db_table = 'sys_applicant_verification'
        verbose_name = 'Solicitante'
        verbose_name_plural = 'Solicitantes'


class VerificationRequest(SoftDeleteModel):
    STATUS = (
        ('E', 'Es espera de verificación'),
        ('A', 'Aprobado'),
        ('R', 'Rechazado'),
        ('C', 'Es espera de aprobación por control previo'),
    )

    birth_certificate = models.FileField(upload_to='actas/', null=False, blank=False, verbose_name='Acta de nacimiento')
    application_date = models.DateField(default=_date.date.today, verbose_name='Fecha de la solicitud')
    status = models.CharField(
        max_length=1,
        choices=STATUS,
        default='E',
        verbose_name='Estatus de la solicitud')
    date_appointment = models.DateField(null=True, blank=True, verbose_name='Fecha de la cita')
    applicant = models.ForeignKey(Applicant, null=False, blank=True, on_delete=models.CASCADE,
                                  verbose_name='Solicitante')

    def __str__(self):
        return 'Solicitud #' + str(self.id)

    def nombre_solicitante(self):
        return self.applicant.first_name

    def apellido_solicitante(self):
        return self.applicant.last_name

    def acta(self):
        return mark_safe('<a href = "/media/' + str(
            self.birth_certificate) + '" target="_blank" class= "text-center"><img src = "/media/' + str(
            self.birth_certificate) + '" width="50"></a>')

    class Meta:
        db_table = 'sys_verification_request'
        verbose_name = 'Solicitud de verificación'
        verbose_name_plural = 'Solicitudes de verificación'
        ordering = ['application_date']
