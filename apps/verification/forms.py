from django import forms
from django.forms import FileInput
from apps.verification.models import VerificationRequest, Applicant


class FormRegistrationApplicant(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo != 'birth_certificate' and campo != 'is_deleted':
                self.fields[campo].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Applicant
        fields = '__all__'


class FormCreateRequestVerification(forms.ModelForm):
    class Meta:
        model = VerificationRequest
        exclude = ['application_date', 'status', 'date_appointment']
        widgets = {
            'birth_certificate':
                FileInput(attrs={'class': 'd-none',
                                 'accept': 'image/*',
                                 'birth_certificate': 'certificate'})
        }
