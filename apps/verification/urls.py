from django.urls import path

from apps.verification.views import CreateVerificationRequest, ListOffice

urlpatterns = [
    path('', ListOffice.as_view()),
    path('crear-solicitud/', CreateVerificationRequest.as_view()),
]
