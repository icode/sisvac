from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import CreateView, ListView
from django_tenants.postgresql_backend.base import IntegrityError

from apps.system.models import DomainOffice
from apps.verification.forms import FormCreateRequestVerification, FormRegistrationApplicant
from apps.verification.models import VerificationRequest
from sisvan import settings


class CreateVerificationRequest(CreateView):
    model = VerificationRequest
    form_class = FormCreateRequestVerification
    form_applicant = FormRegistrationApplicant
    template_name = 'verification/request_verification.html'
    success_url = '/crear-solicitud/'

    def get_context_data(self, **kwargs):
        context = super(CreateVerificationRequest, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class()

        if 'form_applicant' not in context:
            context['form_applicant'] = self.form_applicant()

        context['DOMAIN'] = settings.MAIN_DOMAIN

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST, request.FILES)
        form_applicant = self.form_applicant(request.POST, request.FILES)
        if not form.is_valid() or not form_applicant.is_valid():
            return render(request, self.template_name, self.get_context_data(form=form, form_applicant=form_applicant))

        # try:
        verification_request = form.save(commit=False)
        verification_request.applicant = form_applicant.save(commit=True)
        verification_request.save()
        messages.info(request, 'El envio de la solicitud de verificación se realizó con éxito.')
        return HttpResponseRedirect(self.success_url)

        # except IntegrityError:
        #     applicant = Applicant.objects.get(identification_number=request.POST.get('identification_number'))


class ListOffice(ListView):
    model = DomainOffice
    queryset = DomainOffice.objects.filter(tenant__on_trial=True)
    template_name = 'verification/selected_office.html'

    def get_context_data(self, **kwargs):
        context = super(ListOffice, self).get_context_data(**kwargs)

        context['DOMAIN'] = settings.MAIN_DOMAIN

        return context
