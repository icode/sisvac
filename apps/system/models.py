from django.contrib.auth.models import User
from django.db import models
from django_tenants.models import TenantMixin, DomainMixin


class Offices(TenantMixin):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, null=True, blank=True)
    address = models.TextField(null=True, blank=True, verbose_name='Dirección')
    paid_until = models.DateField()
    on_trial = models.BooleanField()
    users = models.ManyToManyField(User, null=True, blank=True, verbose_name='Usuarios')
    created_on = models.DateField(auto_now_add=True)

    auto_create_schema = True

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'sys_oficinas'
        verbose_name_plural = 'Oficinas'
        verbose_name = 'Oficina'


class DomainOffice(DomainMixin):
    class Meta:
        db_table = 'sys_dominios'
        verbose_name = 'Sitio web'
        verbose_name_plural = 'Sitios web'


# class AttentionSettings(models.Model):
#     WEEKDAY = (
#         ('D', 'Domingo'),
#         ('L', 'Lunes'),
#         ('M', 'Martes'),
#         ('E', 'Miercoles'),
#         ('J', 'Jueves'),
#         ('V', 'Viernes'),
#         ('S', 'Sabado')
#     )
#     NUMBERS = (
#         ('0', '0'),
#         ('1', '1'),
#         ('2', '2'),
#         ('3', '3'),
#         ('4', '4'),
#         ('5', '5'),
#         ('6', '6'),
#         ('7', '7'),
#         ('8', '8'),
#         ('9', '9'),
#     )
#     weekday = models.CharField(max_length=1, choices=WEEKDAY, verbose_name='Día de la semana')
#     final_number_one = models.CharField(max_length=1, choices=NUMBERS, verbose_name='Terminal de cédula 1')
#     final_number_two = models.CharField(max_length=1, verbose_name='Terminal de cédula 2')
#
#     class Meta:
#         db_table = 'sys_attention_settings'
#         verbose_name = 'Configuración de terminal de cédula'
#         verbose_name_plural = 'Configuraciones de terminal de cédula'
