from django.apps import AppConfig


class SistemasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.system'
    verbose_name = 'Sistema'
