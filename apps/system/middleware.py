import re

from django.contrib import messages
from django.utils.deprecation import MiddlewareMixin
from django_tenants.utils import remove_www

from apps.system.models import DomainOffice
from django.http import HttpResponseRedirect


class CheckUserOfficeMiddleware(MiddlewareMixin):
    @staticmethod
    def hostname_from_request(request):
        return remove_www(request.get_host().split(':')[0])

    def process_request(self, request):
        hostname = self.hostname_from_request(request)

        if not request.user.is_superuser and request.user.username != '' \
                and not re.search(r'/login/', request.get_full_path()):
            data = DomainOffice.objects.filter(domain=hostname, tenant__users__username=request.user.username)

            if not data:
                request.session.flush()
                messages.warning(request,
                                 'Usted no tiene permiso para ingresar al portal administrativo de ' + hostname)
                return HttpResponseRedirect('/admin/login/')
