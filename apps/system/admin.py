from django.contrib import admin
from apps.system.models import Offices, DomainOffice

admin.site.register(Offices)


@admin.register(DomainOffice)
class AdminUsersOffices(admin.ModelAdmin):
    list_display = ['domain', 'tenant']
    fieldsets = [
        (None, {
            'fields': [
                ('domain',),
                ('tenant',),
            ]
        })
    ]
