#!/usr/bin python3
from django.core.management.base import BaseCommand

from apps.system.utils import show_console_message


class Command(BaseCommand):
    help = 'Genera una llave secreta de seguridad del sistema'

    def handle(self, *args, **options):
        from django.core.management.utils import get_random_secret_key
        show_console_message(get_random_secret_key())
