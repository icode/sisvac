#!/usr/bin python3
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from datetime import datetime
from apps.system.models import Offices, DomainOffice
from apps.system.utils import show_console_message


class Command(BaseCommand):
    help = 'Creada las configuraciones necesarios para ejecutar la aplicación en ambiente de desarrollo'

    def handle(self, *args, **options):
        self.create_schema_base()
        self.create_office()
        self.create_user_system()

        show_console_message('Configuración ejecutada con éxito.')

    def create_user_system(self):
        User(
            username='desarrollador',
            email='dev@jaspesoft.com',
            password=make_password('12345678'),
            is_staff=True,
            is_superuser=True,
            is_active=True
        ).save()

    def create_schema_base(self):
        tenant = Offices(schema_name='public',
                         name='Núcleo del sistema en desarrollo.',
                         paid_until=datetime.now().date(),
                         on_trial=False)
        tenant.save()

        domain = DomainOffice()
        domain.domain = 'localhost'
        domain.tenant = tenant
        domain.is_primary = False
        domain.save()

    def create_office(self):
        tenant = Offices(schema_name='oficina_dev',
                         name='Oficina de desarrollo',
                         paid_until=datetime.now().date(),
                         on_trial=True)
        tenant.save()

        domain = DomainOffice()
        domain.domain = 'oficina.localhost'
        domain.tenant = tenant
        domain.is_primary = True
        domain.save()
