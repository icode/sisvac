import datetime
import sys
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import OutputWrapper
from django.core.management import color
from django.template.loader import render_to_string
from sisvan import settings
from sisvan.settings import EMAIL_FORM


def show_console_message(msg):
    stdout = OutputWrapper(sys.stdout)
    style = color.color_style()
    stdout.write(style.NOTICE(msg))


def send_email(mail_subject, message, from_email):
    mail = EmailMultiAlternatives(mail_subject, message, EMAIL_FORM, [from_email])
    mail.attach_alternative(message, "text/html")

    mail.send()
    return True


def email_request_accepted(to_email, name, scheduled_date: datetime, office_address: str):
    mail_subject = 'Solicitud de verificación de acta de nacimiento.'
    message = render_to_string('email/request-accepted.html', {
        'scheduled_date': str(scheduled_date.date.today),
        'name': name,
        'office_address': office_address,
        'DOMAIN': settings.MAIN_DOMAIN
    })
    send_email(mail_subject, message, to_email)


def email_request_rejected(to_email, name):
    mail_subject = 'Solicitud de verificación de acta de nacimiento.'
    message = render_to_string('email/request-denied.html', {
        'name': name,
        'DOMAIN': settings.MAIN_DOMAIN
    })
    send_email(mail_subject, message, to_email)
